<?php
#
/**
 * Return if the string (after being trimmed) is _nu_ll or _em_pty
 * @param string $str a string to check
 * @return bool true when the string is null or empty, otherwise false
 */
function nutem($str)
{
 if (($str === null) or trim($str) == '')
	return true;
 else
	return false;
}
#
/**
 * Return if the string (without being trimmed) is _nu_ll or _em_pty
 * @param string $str a string to check
 * @return bool true when the string is null or empty, otherwise false
 */
function nuem($str)
{
 if (($str === null) or $str == '')
	return true;
 else
	return false;
}
#
/**
 * Get a parameter from the POST array.
 * When $both is true, the GET array will be used as a last resort.
 * When $both is true, and a value is found in the POST array,
 * the GET array will not be checked.
 *
 * Note: The value returned is limited to a length of 1024 characters.
 * @param string $name the key of the parameter
 * @param bool $both if GET should be used if key is not found in POST
 * @return null|string the value found or null if not found
 */
function getparam($name, $both)
{
 $a = null;
 if (isset($_POST[$name]))
	$a = $_POST[$name];

 if (($both === true) and ($a === null))
 {
	if (isset($_GET[$name]))
		$a = $_GET[$name];
 }

 if ($a == '' || $a == null)
	return null;

 // limit to 1K just to be sure
 return substr($a, 0, 1024);
}
#
?>
